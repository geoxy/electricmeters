package pl.stepwise.loader

import com.google.api.services.bigquery.model.TableReference
import com.google.api.services.bigquery.model.TableRow
import org.apache.beam.sdk.io.FileIO
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO
import org.apache.beam.sdk.transforms.*
import org.apache.beam.sdk.values.KV
import org.apache.beam.sdk.values.TypeDescriptor
import org.apache.beam.sdk.values.TypeDescriptors
import java.io.BufferedReader
import java.io.InputStreamReader
import java.nio.channels.Channels

typealias FileLine = KV<String, String>
typealias SpitedFileLine = KV<String, List<String>>

object PipelineSteps {
    private const val headers = """"No","year","month","day","hour","PM2.5","PM10","SO2","NO2","CO","O3","TEMP","PRES","DEWP","RAIN","wd","WSPM","station""""

    fun filterHeaderRow(): Filter<FileLine> = Filter.by(ProcessFunction<FileLine, Boolean> { x -> !x.value.startsWith(headers) })

    fun writeToBigQuery(resultTableSpec: TableReference): BigQueryIO.Write<TableRow> {
        return BigQueryIO.writeTableRows()
                .to(resultTableSpec)
                .withSchema(RecordSchema.schema)
                .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND)
    }

    fun splitCsvLine(): MapElements<FileLine, SpitedFileLine> {
        return MapElements.into(TypeDescriptors.kvs(
                TypeDescriptors.strings(),
                TypeDescriptors.lists(
                        TypeDescriptor.of(String::class.java)
                )
        ))
                .via(ProcessFunction<FileLine, SpitedFileLine> { x -> KV.of(x.key, x.value.split(",")) })
    }

    fun mapEntityToTableRow(): MapElements<Record, TableRow> {
        return MapElements.into(TypeDescriptor.of(TableRow::class.java))
                .via(ProcessFunction { r -> RecordSchema.toTableRow(r) })
    }

    fun mapElementsToEntity(): MapElements<SpitedFileLine, Record> {
        return MapElements.into(TypeDescriptor.of(Record::class.java))
                .via(ProcessFunction<SpitedFileLine, Record> { r ->
                    Record(meter = r.key,
                            timestamp = Utils.csvToLocalDateTime(r.value.subList(1, 5)),
                            pm25 = r.value.getOrNull(5)?.toDoubleOrNull(),
                            pm10 = r.value.getOrNull(6)?.toDoubleOrNull(),
                            so2 = r.value.getOrNull(7)?.toDoubleOrNull(),
                            no2 = r.value.getOrNull(8)?.toDoubleOrNull(),
                            co = r.value.getOrNull(9)?.toDoubleOrNull(),
                            o3 = r.value.getOrNull(10)?.toDoubleOrNull(),
                            temp = r.value.getOrNull(11)?.toDoubleOrNull(),
                            press = r.value.getOrNull(12)?.toDoubleOrNull(),
                            dewp = r.value.getOrNull(13)?.toDoubleOrNull(),
                            rain = r.value.getOrNull(14)?.toDoubleOrNull(),
                            wd = r.value.getOrNull(15),
                            wspm = r.value.getOrNull(16)?.toDoubleOrNull()
                    )
                })
    }

    fun readLines(): ParDo.SingleOutput<FileIO.ReadableFile, FileLine> {
        return ParDo.of(object : DoFn<FileIO.ReadableFile, FileLine>() {
            val fileNameRegex = """(PRSA_Data_)([A-Za-z]*)(_[0-9]{8}-[0-9]{8}\.csv)""".toRegex()

            @ProcessElement
            fun process(c: ProcessContext) {
                val file = c.element()
                val meterName: String = fileNameRegex.find(file.metadata.resourceId().toString())?.groupValues?.get(2).orEmpty()
                for (line in BufferedReader(InputStreamReader(Channels.newInputStream(file.open()))).lines()) {
                    c.output(KV.of<String, String>(meterName, line))
                }
            }
        })
    }
}