package pl.stepwise.loader

import com.google.api.services.bigquery.model.TableFieldSchema
import com.google.api.services.bigquery.model.TableRow
import com.google.api.services.bigquery.model.TableSchema
import com.google.common.collect.ImmutableList

object RecordSchema {

    val schema: TableSchema
        get() = TableSchema().setFields(ImmutableList.of(
                TableFieldSchema().setName("meter").setType("STRING").setMode("NULLABLE"),
                TableFieldSchema().setName("timestamp").setType("TIMESTAMP").setMode("NULLABLE"),
                TableFieldSchema().setName("pm25").setType("FLOAT64").setMode("NULLABLE"),
                TableFieldSchema().setName("pm10").setType("FLOAT64").setMode("NULLABLE"),
                TableFieldSchema().setName("so2").setType("FLOAT64").setMode("NULLABLE"),
                TableFieldSchema().setName("no2").setType("FLOAT64").setMode("NULLABLE"),
                TableFieldSchema().setName("co").setType("FLOAT64").setMode("NULLABLE"),
                TableFieldSchema().setName("o3").setType("FLOAT64").setMode("NULLABLE"),
                TableFieldSchema().setName("temp").setType("FLOAT64").setMode("NULLABLE"),
                TableFieldSchema().setName("press").setType("FLOAT64").setMode("NULLABLE"),
                TableFieldSchema().setName("dewp").setType("FLOAT64").setMode("NULLABLE"),
                TableFieldSchema().setName("rain").setType("FLOAT64").setMode("NULLABLE"),
                TableFieldSchema().setName("wd").setType("STRING").setMode("NULLABLE"),
                TableFieldSchema().setName("wspm").setType("FLOAT64").setMode("NULLABLE")
        ))

    fun toTableRow(record: Record): TableRow {
        val row = TableRow()
        row.set("meter", record.meter)
        row.set("timestamp", Utils.toTimeStampBQFormat(record.timestamp))
        row.set("pm25", record.pm25)
        row.set("pm10", record.pm10)
        row.set("so2", record.so2)
        row.set("no2", record.no2)
        row.set("co", record.co)
        row.set("o3", record.o3)
        row.set("temp", record.temp)
        row.set("press", record.press)
        row.set("dewp", record.dewp)
        row.set("rain", record.rain)
        row.set("wd", record.wd)
        row.set("wspm", record.wspm)

        return row
    }
}
