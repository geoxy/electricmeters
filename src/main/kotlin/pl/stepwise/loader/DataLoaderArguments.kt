package pl.stepwise.loader

import org.apache.commons.cli.Option
import org.apache.commons.cli.Options

fun buildArgumentOptions(): Options {
    val options = Options()
    options.addOption(Option.builder().argName("runner").longOpt("runner").hasArg().desc("Apache Beam runner").required().build())
    options.addOption(Option.builder().argName("project").longOpt("project").hasArg().desc("GCP project name").required().build())
    options.addOption(Option.builder().argName("input").longOpt("input").hasArg().desc("JSON file, which contains calendar-events collection from Office365").required().build())
    options.addOption(Option.builder().argName("job_name").longOpt("job_name").hasArg().desc("Job name").required().build())
    options.addOption(Option.builder().argName("dataset").longOpt("dataset").hasArg().desc("Big Query dataset name").required().build())
    options.addOption(Option.builder().argName("temp_location").longOpt("temp_location").hasArg().desc("Dataflow temp location (path to any GCP bucket eg. gs://INPUT_BUCKET/temp)").required().build())
    options.addOption(Option.builder().argName("staging_location").longOpt("staging_location").hasArg().desc("Dataflow staging location (path to any GCP bucket eg. gs://INPUT_BUCKET/staging)").required().build())
    options.addOption(Option.builder().argName("table_name").longOpt("table_name").hasArg().desc("Big Query result table name").required().build())

    return options
}
