package pl.stepwise.loader

import java.time.DateTimeException
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

object Utils {

    private val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")

    fun toTimeStampBQFormat(timestamp: LocalDateTime?): String? {
        return try {
            timestamp?.format(dateTimeFormatter)
        } catch (e: DateTimeException) {
            null
        }
    }

    fun csvToLocalDateTime(r: List<String>): LocalDateTime? {
        return try {
            if (r.getOrNull(0) == null
                    || r.getOrNull(1) == null
                    || r.getOrNull(2) == null
                    || r.getOrNull(3) == null) {
                return null
            }
            LocalDateTime.of(r[0].toInt(), r[1].toInt(), r[2].toInt(), r[3].toInt(), 0)
        } catch (e: DateTimeException) {
            null
        }
    }
}
