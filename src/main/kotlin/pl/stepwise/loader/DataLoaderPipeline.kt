package pl.stepwise.loader

import com.google.api.services.bigquery.model.TableReference
import org.apache.beam.sdk.Pipeline
import org.apache.beam.sdk.io.FileIO


object DataLoaderPipeline {

    fun process(pipeline: Pipeline, parameters: DataLoaderParameters): Pipeline {
        val resultTableSpec = TableReference()
                .setProjectId(parameters.projectId)
                .setDatasetId(parameters.dataset)
                .setTableId(parameters.tableId)

        pipeline
                .apply("Files match pattern", FileIO.match().filepattern(parameters.inputFilePath))
                .apply("Prepare csv files", FileIO.readMatches())
                .apply("Read lines", PipelineSteps.readLines())
                .apply("Filter headers", PipelineSteps.filterHeaderRow())
                .apply("Split lines", PipelineSteps.splitCsvLine())
                .apply("Map to entity", PipelineSteps.mapElementsToEntity())
                .apply("Map to table row", PipelineSteps.mapEntityToTableRow())
                .apply("Write to BigQuery", PipelineSteps.writeToBigQuery(resultTableSpec))

        return pipeline
    }
}